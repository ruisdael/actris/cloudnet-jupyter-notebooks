# Jupyter Notebooks - exploring ACTRIS Cloudnet Data Portal

ACTRIS Cloudnet Data Portal API reference:
https://docs.cloudnet.fmi.fi/api/data-portal.html 


Base URL https://cloudnet.fmi.fi/api/

## Notebooks

* [Cloudnet_api.ipynb](Cloudnet_api.ipynb) 
   * Raw-files and Products-files 